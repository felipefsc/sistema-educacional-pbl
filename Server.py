from flask import Flask, jsonify, request, render_template
import json

app = Flask(__name__)

##CONTROLE DE ALUNOS
alunos_json = "dados/alunos.json"

def carregar_alunos():
    with open(alunos_json, "r", encoding="utf-8") as arquivoAlunos:
        return json.load(arquivoAlunos)

def salvar_alunos(lista_alunos):
    with open(alunos_json, "w", encoding="utf-8") as arquivoAlunos:
        json.dump(lista_alunos, arquivoAlunos, ensure_ascii=False, indent=4)

lista_alunos = carregar_alunos()

# Rota para renderizar a página ControleAluno.html
@app.route('/alunos', methods=['GET'])
def get_alunos():
    return jsonify(lista_alunos)

# Pesquisar aluno pelo ID
@app.route('/alunos/<int:aluno_id>', methods=['GET'])
def get_aluno(aluno_id):
    for aluno in lista_alunos:
        if aluno['idAluno'] == aluno_id:
            return aluno
        
    return {'Erro': 'Aluno não encontrado'}

# Inserir registro de aluno
@app.route('/alunos/registrar', methods=['POST'])
def registrar_aluno():
    novo_aluno = {
        'idAluno': len(lista_alunos) + 1,
        'nome': request.json['nome'],
        'RA': request.json['RA'],
        'data_de_nascimento': request.json['data_de_nascimento'],
        'email': request.json['email'],
        'cpf': request.json['cpf'],
        'idTurma': request.json['idTurma']
    }
    lista_alunos.append(novo_aluno)
    salvar_alunos(lista_alunos)
    return novo_aluno

# Atualizar registro de aluno
@app.route('/alunos/editar/<int:aluno_id>', methods=['PUT'])
def atualizar_aluno(aluno_id):
    for aluno in lista_alunos:
        if aluno['idAluno'] == aluno_id:
            aluno['nome'] = request.json['nome']
            aluno['RA'] = request.json['RA']
            aluno['data_de_nascimento'] = request.json['data_de_nascimento']
            aluno['email'] = request.json['email']
            aluno['cpf'] = request.json['cpf']
            aluno['idTurma'] = request.json['idTurma']
            salvar_alunos(lista_alunos)
            return aluno

    return {'Erro': 'Aluno não encontrado'}

#Excluir registro de aluno
@app.route('/alunos/excluir/<int:aluno_id>', methods=['DELETE'])
def apagar_aluno(aluno_id):
    aluno_encontrado = None
    for aluno in lista_alunos:
        if aluno['idAluno'] == aluno_id:
            aluno_encontrado = aluno
            break

    if aluno_encontrado:
        lista_alunos.remove(aluno_encontrado)
        salvar_alunos(lista_alunos)
        return {'Mensagem': 'Registro de aluno excluído com sucesso'}
    
    return {'Erro': 'Aluno não encontrado'}

##CONTROLE DE TURMAS
turmas_json = "dados/turmas.json"

def carregar_turmas():
    with open(turmas_json, "r", encoding="utf-8") as arquivoTurmas:
        return json.load(arquivoTurmas)

def salvar_turmas(lista_turmas):
    with open(turmas_json, "w", encoding="utf-8") as arquivoTurmas:
        json.dump(lista_turmas, arquivoTurmas, ensure_ascii=False, indent=4)

lista_turmas = carregar_turmas()

# Listar turmas
@app.route('/turmas', methods=['GET'])
def get_turmas():
    return jsonify(lista_turmas)

# Pesquisar turma pelo ID
@app.route('/turmas/<int:turma_id>', methods=['GET'])
def get_turma(turma_id):
    for turma in lista_turmas:
        if turma['idTurma'] == turma_id:
            return turma
        
    return {'Erro': 'Turma não encontrada'}

# Inserir registro de turma
@app.route('/turmas/registrar', methods=['POST'])
def registrar_turma():
    nova_turma = {
        'idTurma': len(lista_turmas) + 1,
        'registro': request.json['registro'],
        'curso': request.json['curso'],
        'periodo': request.json['periodo']
    }
    lista_turmas.append(nova_turma)
    salvar_turmas(lista_turmas)
    return nova_turma

# Atualizar registro de turma
@app.route('/turmas/editar/<int:turma_id>', methods=['PUT'])
def atualizar_turma(turma_id):
    for turma in lista_turmas:
        if turma['idTurma'] == turma_id:
            turma['registro'] = request.json['registro']
            turma['matricula'] = request.json['matricula']
            turma['horario'] = request.json['horario']
            salvar_turmas(lista_turmas)
            return turma

    return {'Erro': 'Turma não encontrada'}

@app.route('/turmas/<int:turma_id>', methods=['DELETE'])
def apagar_turma(turma_id):
    turma_encontrada = None
    for turma in lista_turmas:
        if turma['idTurma'] == turma_id:
            turma_encontrada = turma
            break

    if turma_encontrada:
        lista_turmas.remove(turma_encontrada)
        salvar_turmas(lista_turmas)
        return {'Mensagem': 'Registro de turma excluído com sucesso'}
    
    return {'Erro': 'Turma não encontrada'}

##UPLOAD DE ARQUIVOS
@app.route('/upload', methods=['POST'])
def upload_alunos():
    import os
    arquivo_importado = request.files['arquivo']
    if arquivo_importado and arquivo_validado(arquivo_importado.filename):
        pastaDestino = os.path.join('dados/',arquivo_importado.filename)
        arquivo_importado.save(pastaDestino)
        return {'data':'Upload feito com sucesso'}

    else:
        return {'error':'Upload não funcionou ou extensão de arquivo não permitida'}

def arquivo_validado(filename):
    EXTENSOES = ['json']
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in EXTENSOES

# Executar a aplicação Flask
if __name__ == '__main__':
    app.run(debug=True)